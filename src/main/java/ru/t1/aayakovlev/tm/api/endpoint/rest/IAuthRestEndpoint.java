package ru.t1.aayakovlev.tm.api.endpoint.rest;

import org.jetbrains.annotations.NotNull;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;
import ru.t1.aayakovlev.tm.dto.model.UserDTO;

@RestController
@RequestMapping("/api/auth")
public interface IAuthRestEndpoint {

    @PostMapping(value = "/login", consumes = MediaType.MULTIPART_FORM_DATA_VALUE)
    boolean login(
            @RequestParam("username") @NotNull final String username,
            @RequestParam("password") @NotNull final String password
    );

    @GetMapping("/logout")
    boolean logout();

    @NotNull
    @GetMapping("/profile")
    UserDTO profile();

}
