package ru.t1.aayakovlev.tm.config;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.jetbrains.annotations.NotNull;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.authentication.www.BasicAuthenticationEntryPoint;
import ru.t1.aayakovlev.tm.dto.model.MessageDTO;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;

public class AuthenticationEntryPoint extends BasicAuthenticationEntryPoint {

    @Override
    public void commence(
            @NotNull final HttpServletRequest request,
            @NotNull final HttpServletResponse response,
            @NotNull final AuthenticationException exception
    ) throws IOException {
        response.setStatus(HttpServletResponse.SC_UNAUTHORIZED);
        @NotNull final PrintWriter writer = response.getWriter();
        @NotNull final ObjectMapper mapper = new ObjectMapper();
        @NotNull final String value = exception.getMessage();

        @NotNull final MessageDTO message = new MessageDTO(value);
        writer.println(mapper.writerWithDefaultPrettyPrinter().writeValueAsString(message));
    }

    @Override
    public void afterPropertiesSet() {
        setRealmName("aayakovlev");
        super.afterPropertiesSet();
    }

}
