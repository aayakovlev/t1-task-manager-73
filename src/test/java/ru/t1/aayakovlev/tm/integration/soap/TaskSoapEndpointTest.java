package ru.t1.aayakovlev.tm.integration.soap;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.*;
import org.junit.experimental.categories.Category;
import org.springframework.data.util.CastUtils;
import org.springframework.http.HttpHeaders;
import ru.t1.aayakovlev.tm.api.endpoint.soap.IAuthSoapEndpoint;
import ru.t1.aayakovlev.tm.api.endpoint.soap.ITaskSoapEndpoint;
import ru.t1.aayakovlev.tm.client.soap.AuthSoapEndpointClient;
import ru.t1.aayakovlev.tm.client.soap.TaskSoapEndpointClient;
import ru.t1.aayakovlev.tm.dto.model.TaskDTO;
import ru.t1.aayakovlev.tm.dto.soap.*;
import ru.t1.aayakovlev.tm.enumerated.Status;
import ru.t1.aayakovlev.tm.marker.IntegrationCategory;

import javax.xml.ws.BindingProvider;
import javax.xml.ws.handler.MessageContext;
import java.net.MalformedURLException;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static ru.t1.aayakovlev.tm.constant.dto.TaskDTOTestConstant.*;

@Category(IntegrationCategory.class)
public class TaskSoapEndpointTest {

    @NotNull
    private static final String URL = "http://localhost:8080";

    @NotNull
    private static IAuthSoapEndpoint authEndpoint;

    @NotNull
    private static ITaskSoapEndpoint taskEndpoint;

    @Nullable
    private static String USER_ID;

    @BeforeClass
    public static void beforeClass() throws MalformedURLException {
        authEndpoint = AuthSoapEndpointClient.getInstance(URL);
        Assert.assertTrue(authEndpoint.login(new AuthLoginRequest("test", "test")).isResult());
        taskEndpoint = TaskSoapEndpointClient.getInstance(URL);
        @NotNull final BindingProvider authBindingProvider = (BindingProvider) authEndpoint;
        @NotNull final BindingProvider taskBindingProvider = (BindingProvider) taskEndpoint;
        @NotNull Map<String, List<String>> headers = CastUtils.cast((Map) authBindingProvider.getResponseContext().get(MessageContext.HTTP_RESPONSE_HEADERS));
        if (headers == null) headers = new HashMap<String, List<String>>();
        @NotNull final Object cookieValue = headers.get(HttpHeaders.SET_COOKIE);
        @NotNull final List<String> cookies = (List<String>) cookieValue;
        headers.put("Cookie", Collections.singletonList(cookies.get(0)));
        taskBindingProvider.getRequestContext().put(MessageContext.HTTP_REQUEST_HEADERS, headers);
        USER_ID = authEndpoint.profile(new AuthProfileRequest()).getUser().getId();
    }

    @Before
    public void before() throws Exception {
        TASK_ONE.setUserId(USER_ID);
        TASK_TWO.setUserId(USER_ID);
        TASK_THREE.setUserId(USER_ID);
        taskEndpoint.save(new TaskSaveRequest(TASK_ONE));
        taskEndpoint.save(new TaskSaveRequest(TASK_TWO));
    }

    @Test
    public void findAllTest() throws Exception {
        @Nullable final List<TaskDTO> tasks = taskEndpoint.findAll(new TaskFindAllRequest()).getTask();
        Assert.assertEquals(2, tasks.size());
    }

    @After
    public void after() throws Exception {
        taskEndpoint.deleteAll(new TaskDeleteAllRequest());
    }

    @Test
    public void addTest() throws Exception {
        taskEndpoint.save(new TaskSaveRequest(TASK_THREE));
        Assert.assertNotNull(taskEndpoint.findById(new TaskFindByIdRequest(TASK_THREE.getId())));
    }

    @Test
    public void saveTest() throws Exception {
        TASK_ONE.setStatus(Status.IN_PROGRESS);
        taskEndpoint.save(new TaskSaveRequest(TASK_ONE));
        @Nullable final TaskDTO task = taskEndpoint.findById(new TaskFindByIdRequest(TASK_ONE.getId())).getTask();
        Assert.assertNotNull(task);
        Assert.assertEquals(Status.IN_PROGRESS, task.getStatus());
    }

    @Test
    public void findByIdTest() throws Exception {
        Assert.assertNotNull(taskEndpoint.findById(new TaskFindByIdRequest(TASK_ONE.getId())));
    }

    @Test
    public void deleteTest() throws Exception {
        taskEndpoint.deleteById(new TaskDeleteByIdRequest(TASK_ONE.getId()));
        Assert.assertNotNull(taskEndpoint.findById(new TaskFindByIdRequest(TASK_ONE.getId())));
    }

    @Test
    public void deleteAllTest() throws Exception {
        taskEndpoint.deleteAll(new TaskDeleteAllRequest());
        Assert.assertNotNull(taskEndpoint.findAll(new TaskFindAllRequest()));
    }

}
