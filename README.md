# TASK MANAGER

Simple application for project and task management.

## DEVELOPER

**NAME**: Yakovlev Anton

**E-MAIL**: aayakovlev@t1-consulting.ru

## SOFTWARE

**OS**: Windows 10 LTSC 1809 (build 17763.2628)

**JDK**: OpenJDK 1.8.0_41

**MAVEN**: 3.6.3

## HARDWARE

**CPU**: AMD R9

**RAM**: 32 Gb

**SSD**: NVMe 512 Gb

## APPLICATION PROPERTIES

| OS ENV             | JAVA OPTS            | DEFAULT VALUE                                 | MEANING                  |
|--------------------|----------------------|-----------------------------------------------|--------------------------|
| DATABASE_URL       | -Ddatabase.url       | jdbc:postgresql://127.0.0.1:5432/task_manager | database connection url  |
| DATABASE_USERNAME  | -Ddatabase.username  | postgres                                      | database user            |
| DATABASE_PASSWORD  | -Ddatabase.password  | postgres                                      | database password        |

## BUILD APPLICATION

````shell
mvn clean install
````

## RUN APPLICATION

### FROM IDE

````shell
mvn tomcat7:run-war
````

### STANDALONE

place task-manager-web.war into running tomcat deploy directory.
